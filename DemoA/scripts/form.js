

var initialLoad = true;
$(function() {
	initialSetup();
	initialLoad = false;
	
	$("#next").click(function(){
		nextStep();
	});
	
	$("#prev").click(function() {
		prevStep();
	});
	
	$("#finnish").click(function() {
		window.location.href="complete.html";
	});

	$("#charity").change(function() {
		if($('#charity').is(':checked'))
		{
			$("#charityref").show();
		}
		else
		{
			$("#charityref").hide();
		}
	});

	$("#company").change(function() {
		if($('#company').is(':checked'))
		{
			$("#companyref").show();
			$(".companyonly").show();
		}
		else
		{
			$("#companyref").hide();
			$(".companyonly").hide();
		}
	});
    
	$("#other").change(function(){
		if($('#other').is(':checked'))
		{
			$('#specify').show();
		}
		else
		{
           $('#specify').hide();
		}
	});

	$('input[type=radio][name=boardMembers]').change(function() {
		if (this.value == 'no') {
           $("#ineligible1").dialog({
              modal: true,
              buttons: {
                "Exit Application": function() {
                $( this ).dialog( "close" );
				window.location.href="ineligible.html";
             },
             "Correct Application": function() {
		     $('input[type=radio][name=boardMembers]').each(function () { $(this).prop('checked', false); });
             $( this ).dialog( "close" );
             }
			  },
			  close: function(){
				  if($('input[type=radio][name=boardMembers]').is(':checked'))
					window.location.href="ineligible.html";
			  }
      
		   });
		}
	});

	$('input[type=radio][name=OrgType]').change(function() {
		if (this.value == 'no') {
           $("#ineligible2").dialog({
              modal: true,
              buttons: {
                "Exit Application": function() {
                $( this ).dialog( "close" );
				window.location.href="ineligible.html";
             },
             "Correct Application": function() {
		     $('input[type=radio][name=OrgType]').each(function () { $(this).prop('checked', false); });
             $( this ).dialog( "close" );
             }
			  },
			  close: function(){
				  if($('input[type=radio][name=OrgType]').is(':checked'))
					window.location.href="ineligible.html";
			  }
      
		   });
		}

	});

		$('input[type=radio][name=accounts]').change(function() {
		if (this.value == 'no') {
           $("#ineligible4").dialog({
              modal: true,
              buttons: {
                "Exit Application": function() {
                $( this ).dialog( "close" );
				window.location.href="ineligible.html";
             },
             "Correct Application": function() {
		     $('input[type=radio][name=accounts]').each(function () { $(this).prop('checked', false); });
             $( this ).dialog( "close" );
             }
			  },
			  close: function(){
				  if($('input[type=radio][name=accounts]').is(':checked'))
					window.location.href="ineligible.html";
			  }
      
		   });
		}
	});

	$('input[type=radio][name=bankAcc]').change(function() {
		if (this.value == 'no') {
           $("#ineligible3").dialog({
              modal: true,
              buttons: {
                "Exit Application": function() {
                $( this ).dialog( "close" );
				window.location.href="ineligible.html";
             },
             "Correct Application": function() {
		     $('input[type=radio][name=bankAcc]').each(function () { $(this).prop('checked', false); });
             $( this ).dialog( "close" );
             }
			  },
			  close: function(){
				  if($('input[type=radio][name=bankAcc]').is(':checked'))
					window.location.href="ineligible.html";
			  }
      
		   });
		}
	});

	$('input[type=radio][name=amount]').change(function() {
		if (this.value == 'no') {
           $("#ineligible5").dialog({
              modal: true,
              buttons: {
                "Exit Application": function() {
                $( this ).dialog( "close" );
				window.location.href="ineligible.html";
             },
             "Correct Application": function() {
		     $('input[type=radio][name=amount]').each(function () { $(this).prop('checked', false); });
             $( this ).dialog( "close" );
             }
			  },
			  close: function(){
				  if($('input[type=radio][name=amount]').is(':checked'))
					window.location.href="ineligible.html";
			  }
      
		   });
		}
	});

	$('input[type=radio][name=addressyears]').change(function() {
		if (this.value == 'no') {
			$(".prevAddress").show();
		}
		else
		{
			$(".prevAddress").hide();
		}
	});
});

function initialSetup()
{
	if (initialLoad)
	{
		$("#step_2_progress").attr("hidden", true);
	    $("#step_3_progress").attr("hidden", true);
	    $("#step_4_progress").attr("hidden", true);
	    $("#step_5_progress").attr("hidden", true);
	    $("#step_6_progress").attr("hidden", true);
	    $("#step_7_progress").attr("hidden", true);
	    $("#step_8_progress").attr("hidden", true);
	    $("#part2").hide();
	    $("#part3").hide();
	    $("#part4").hide();
	    $("#part5").hide();
	    $("#part6").hide();
	    $("#part7").hide();
	    $("#part8").hide();
	    $("#prev").hide();
	    $("#finnish").hide();
		$("#ineligible1").hide();
		$("#ineligible2").hide();
		$("#ineligible3").hide();
		$("#ineligible4").hide();
		$("#ineligible5").hide();
		$(".additionalinfo").hide();
		$(".prevAddress").hide();
		$('.walesonly').hide();
		$('.nionly').hide();
		$('.companyonly').hide();
		$(".tooltiptext").hide();
		$("#tooltipimg1").hover(function()
		{
			$("#tooltiptext1").show();
		});
		$("#tooltipimg1").mouseout(function()
		{
			$("#tooltiptext1").hide();
		});
		$("#tooltipimg2").hover(function()
		{
			$("#tooltiptext2").show();
		});
		$("#tooltipimg2").mouseout(function()
		{
			$("#tooltiptext2").hide();
		});
		$("#tooltipimg3").hover(function()
		{
			$("#tooltiptext3").show();
		});
		$("#tooltipimg3").mouseout(function()
		{
			$("#tooltiptext3").hide();
		});
		$("#tooltipimg4").hover(function()
		{
			$("#tooltiptext4").show();
		});
		$("#tooltipimg4").mouseout(function()
		{
			$("#tooltiptext4").hide();
		});
		$("#tooltipimg5").hover(function()
		{
			$("#tooltiptext5").show();
		});
		$("#tooltipimg5").mouseout(function()
		{
			$("#tooltiptext5").hide();
		});
		$("#tooltipimg6").hover(function()
		{
			$("#tooltiptext6").show();
		});
		$("#tooltipimg6").mouseout(function()
		{
			$("#tooltiptext6").hide();
		});
		$("#tooltipimg7").hover(function()
		{
			$("#tooltiptext7").show();
		});
		$("#tooltipimg7").mouseout(function()
		{
			$("#tooltiptext7").hide();
		});
		$("#tooltipimg8").hover(function()
		{
			$("#tooltiptext8").show();
		});
		$("#tooltipimg8").mouseout(function()
		{
			$("#tooltiptext8").hide();
		});
		$("#tooltipimg9").hover(function()
		{
			$("#tooltiptext9").show();
		});
		$("#tooltipimg9").mouseout(function()
		{
			$("#tooltiptext9").hide();
		});
		$("#tooltipimg10").hover(function()
		{
			$("#tooltiptext10").show();
		});
		$("#tooltipimg10").mouseout(function()
		{
			$("#tooltiptext10").hide();
		});

		//check query substring
		var ft = qs("formtype");
		if(ft)
		{
			if(ft === "welsh")
			{
				$(".walesonly").show();
			}
			if(ft === "ni")
			{
				$(".nionly").show();
			}
		}

		$("#idea").counter({
          type: 'word',
          goal: 500
        });
	}
	
}

function nextStep()
{
	var step1Hidden = $("#step_1_progress").attr("hidden");
	var step2Hidden = $("#step_2_progress").attr("hidden");
	var step3Hidden = $("#step_3_progress").attr("hidden");
	var step4Hidden = $("#step_4_progress").attr("hidden");
	var step5Hidden = $("#step_5_progress").attr("hidden");
	var step6Hidden = $("#step_6_progress").attr("hidden");
	var step7Hidden = $("#step_7_progress").attr("hidden");
	var step8Hidden = $("#step_8_progress").attr("hidden");
	
		if(typeof(step1Hidden) === typeof(undefined))
	    {
		  
			 $("#step_1_progress").attr("hidden", true);
		     $("#part1").hide();
		     $("#step_2_progress").removeAttr("hidden");
		     $("#part2").show();
		     $("#prev").show();
		     window.scrollTo(0, 0); 
	    }
		else if (typeof(step2Hidden) == typeof(undefined))
		{
		 
			$("#step_2_progress").attr("hidden", true);
		    $("#part2").hide();
		    $("#step_3_progress").removeAttr("hidden");
		    $("#part3").show();
		    $("#prev").show();
			$("#yearEnd").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				yearRange: "-5:+00"
			});
		    window.scrollTo(0, 0);  
		}
		else if (typeof(step3Hidden) == typeof(undefined))
		{
			
		    $("#step_3_progress").attr("hidden", true);
		    $("#part3").hide();
		    $("#step_4_progress").removeAttr("hidden");
		    $("#part4").show();
		    $("#prev").show();
			$(".cddob").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true,
				yearRange: "-90:+00"
			});
		    window.scrollTo(0, 0); 
		 
		}
		else if (typeof(step4Hidden) === typeof(undefined))
		{
			$("#step_4_progress").attr("hidden", true);
			$("#part4").hide();
			$("#step_5_progress").removeAttr("hidden");
			$("#part5").show();
			$("#prev").show();
			$(".psdate").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				yearRange: "-00:+2"
			});
			window.scrollTo(0, 0);
		}
		else if (typeof(step5Hidden) == typeof(undefined))
		{
			$("#step_5_progress").attr("hidden", true);
			$("#part5").hide();
			$("#step_6_progress").removeAttr("hidden");
			$("#part6").show();
			$("#prev").show();
			window.scrollTo(0, 0);
		}
		else if (typeof(step6Hidden) === typeof(undefined))
		{
			$("#step_6_progress").attr("hidden", true);
			$("#part6").hide();
			$("#step_7_progress").removeAttr("hidden");
			$("#part7").show();
			$("#prev").show();
			window.scrollTo(0, 0);
		}
		else if (typeof(step7Hidden) == typeof(undefined))
		{
			$("#step_7_progress").attr("hidden", true);
			$("#part7").hide();
			$("#step_8_progress").removeAttr("hidden");
			$("#part8").show();
			$("#prev").show();
			$("#finnish").show();
			$("#next").hide();
			$(".gadate").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true,
				yearRange: "-1:+1"
			});
			window.scrollTo(0, 0);
		}
		
}

function prevStep()
{
	var step1Hidden = $("#step_1_progress").attr("hidden");
	var step2Hidden = $("#step_2_progress").attr("hidden");
	var step3Hidden = $("#step_3_progress").attr("hidden");
	var step4Hidden = $("#step_4_progress").attr("hidden");
	var step5Hidden = $("#step_5_progress").attr("hidden");
	var step6Hidden = $("#step_6_progress").attr("hidden");
	var step7Hidden = $("#step_7_progress").attr("hidden");
	var step8Hidden = $("#step_8_progress").attr("hidden");
	
	if(typeof(step2Hidden) === typeof(undefined))
	    {
		  $("#step_2_progress").attr("hidden", true);
		  $("#part2").hide();
		  $("#prev").hide();
		  $("#part1").show();
		  $("#step_1_progress").removeAttr("hidden");
		  window.scrollTo(0, 0);
	    }
	else if(typeof(step3Hidden) == typeof(undefined))
	{
		$("#step_3_progress").attr("hidden", true);
		$("#part3").hide();
		$("#part2").show();
		$("#step_2_progress").removeAttr("hidden");
		window.scrollTo(0, 0);
	}
	else if(typeof(step4Hidden) === typeof(undefined))
	{
		$("#step_4_progress").attr("hidden", true);
		$("#part4").hide();
		$("#part3").show();
		$("#step_3_progress").removeAttr("hidden");
		window.scrollTo(0, 0);
	}
	else if(typeof(step5Hidden) == typeof(undefined))
	{
		$("#step_5_progress").attr("hidden", true);
		$("#part5").hide();
		$("#part4").show();
		$("#step_4_progress").removeAttr("hidden");
		window.scrollTo(0, 0);
	}
	else if(typeof(step6Hidden) === typeof(undefined))
	{
		$("#step_6_progress").attr("hidden", true);
		$("#part6").hide();
		$("#part5").show();
		$("#step_5_progress").removeAttr("hidden");
		window.scrollTo(0, 0);
	}
	else if(typeof(step7Hidden) === typeof(undefined))
	{
		$("#step_7_progress").attr("hidden", true);
		$("#part7").hide();
		$("#part6").show();
		$("#step_6_progress").removeAttr("hidden");
		window.scrollTo(0, 0);
	}
	else if(typeof(step8Hidden) === typeof(undefined))
	{
		$("#step_8_progress").attr("hidden", true);
		$("#part8").hide();
		$("#part7").show();
		$("#step_7_progress").removeAttr("hidden");
		$("#finnish").hide();
		$("#next").show();
		window.scrollTo(0, 0);
	}
}


function title_is_selected()
{
	var mrchecked = $("#mr").is(':checked');
	var mrschecked = $("#mrs").is(':checked');
	var mschecked = $("#ms").is(':checked');
	var misschecked = $("#miss").is(':checked');
	
	if (mrchecked||mrschecked||mschecked||misschecked)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function contains_min_alpha_numeric_characters(value, valid)
{
	const hasRequiredChars = 
	/^(?:[^a-zA-Z0-9]*[a-zA-Z0-9]){3}/;
	return hasRequiredChars.test(value);
}

function qs(name) {
    url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
